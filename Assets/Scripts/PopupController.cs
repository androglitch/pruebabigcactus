using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupController : MonoBehaviour
{
    public Text text;

    public void SetText(string newText) {
        this.text.text = newText;
    }

    public void Close() {
        gameObject.SetActive(false);
    }
}

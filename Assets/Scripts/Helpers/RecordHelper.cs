using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class RecordHelper
{
    public string name;
    public List<ButtonStatus[]> records;
}

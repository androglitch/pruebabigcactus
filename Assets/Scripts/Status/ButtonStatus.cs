using System;
using UnityEngine;

[Serializable]
public class ButtonStatus
{
    public uint currentColor;
    public bool isOverButton;
    public bool isDragging;
    public bool showingTooltip;
    public Vector3 objectPosition;
}

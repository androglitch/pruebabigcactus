using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.IO;

public class RecordController : MonoBehaviour
{
    public Text recordButtonText;
    public InputField input;
    public Button loadButton;
    public ButtonController[] buttons;

    private bool _isRecording = false;
    private bool _isPlaying = false;
    private List<ButtonStatus[]> statuses;
    private List<RecordHelper> previousRecords;
    private int _indexRecord;


    private void Start(){
        this._LoadPreviousRecords();
    }

    void Update(){
        this._Record();
        this._PlayRecord();
    }

    public void RecordButtonClick() {
        if (!this._isRecording)
            this._StartRecording();
        else
            this._EndRecording();

    }

    public void PlayRecordButtonClick() {
        this._isPlaying = true;
        this._indexRecord = 0;
    }

    private void _StartRecording() {
        if (this.input.text.Length == 0)
            return;
        this.statuses = new List<ButtonStatus[]>();
        this._isRecording = true;
        this.recordButtonText.text = "Stop Recording";
        this.loadButton.enabled = false;
    }

    private void _Record() {
        if (!this._isRecording)
            return;

        ButtonStatus[] currentStatus = new ButtonStatus[buttons.Length];
        for(int i = 0; i < buttons.Length; i++) {
            currentStatus[i] = buttons[i].GetStatus();
        }
        statuses.Add(currentStatus);
    }

    private void _EndRecording() {
        this._SaveRecord();
        this._isRecording = false;
        this.recordButtonText.text = "Start Recording";
        this.loadButton.enabled = true;
    }

    private void _SaveRecord() {
        RecordHelper helper = new RecordHelper();
        helper.name = this.input.text;
        helper.records = this.statuses;
        this.previousRecords.Add(helper);
        string json = JsonConvert.SerializeObject(previousRecords);
        string destination = Application.persistentDataPath + "/game.json";
        File.WriteAllText(destination, json);

        this.input.text = "";
    }

    private void _PlayRecord() {
        if (!this._isPlaying)
            return;

        if(this._indexRecord >= this.statuses.Count){
            this._isPlaying = false;
            return;
        }
        ButtonStatus[] recordedStatus = this.statuses[this._indexRecord++];
        for (int i = 0; i < buttons.Length; i++){
            buttons[i].SetStatus(recordedStatus[i]);
        }
    }

    private void _LoadPreviousRecords() {
        string origin = Application.persistentDataPath + "/game.json";
        this.previousRecords = new List<RecordHelper>();
        if (File.Exists(origin)){
            string json = File.ReadAllText(origin);
            if (json.Length > 0){
                this.previousRecords = JsonConvert.DeserializeObject<List<RecordHelper>>(json);
            }
        }
    }
}
